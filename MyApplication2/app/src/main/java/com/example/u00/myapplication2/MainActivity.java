package com.example.u00.myapplication2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements TextWatcher {

    private EditText etUser;
    private EditText etPass;
    private Button btIngresar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUser= findViewById(R.id.etUser);
        etPass =findViewById(R.id.etPass);
        btIngresar =findViewById(R.id.btIngresar);
        etUser.addTextChangedListener(this);
        etPass.addTextChangedListener(this);

        btIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CalculadoraActivity.class);
                startActivity(intent);

            }
        });


    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (etUser.getText().toString().trim().isEmpty() || etPass.getText().toString().isEmpty()){
            btIngresar.setEnabled(false);
        }else{
            btIngresar.setEnabled(true);
        }
    }

    public void login(View view) {
        Intent intent = new Intent(MainActivity.this,MenuActivity.class);
        intent.putExtra("nombre", etUser.getText().toString());
        startActivity(intent);
    }
}
