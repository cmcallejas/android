package com.example.u00.myapplication2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CalculadoraActivity extends AppCompatActivity {

    private EditText etNum1;
    private EditText etNum2;
    private Button btnSumar;
    private Button btnRestar;
    private Button btnDividir;
    private Button btnMultiplicar;
    private EditText etResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        etNum1 = findViewById(R.id.etNumero1);
        etNum2 = findViewById(R.id.etNumero2);
        etResultado = findViewById(R.id.etResultado);
        btnSumar= findViewById(R.id.btSumar);
        btnRestar= findViewById(R.id.btRestar);
        btnDividir= findViewById(R.id.btDividir);
        btnMultiplicar= findViewById(R.id.btMultiplicar);

       /* btncalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             System.err.print("hola");
            }
        });*/
    }

    public void calcular(View view) {



    }

    public void dividir(View view) {
        int val1 = Integer.parseInt(etNum1.getText().toString());
        int val2 = Integer.parseInt(etNum2.getText().toString());
        try {
            etResultado.setText(String.valueOf(val1 / val2));
        }catch (ArithmeticException ae){
            Toast.makeText(this,"No se puede dividir por CERO",Toast.LENGTH_LONG);
        }
    }

    public void multiplicar(View view) {
        int val1 = Integer.parseInt(etNum1.getText().toString());
        int val2 = Integer.parseInt(etNum2.getText().toString());

        etResultado.setText(String.valueOf(val1 * val2));
    }

    public void sumar(View view) {
        int val1 = Integer.parseInt(etNum1.getText().toString());
        int val2 = Integer.parseInt(etNum2.getText().toString());
        etResultado.setText(String.valueOf(val1 + val2));
    }

    public void restar(View view) {
        int val1 = Integer.parseInt(etNum1.getText().toString());
        int val2 = Integer.parseInt(etNum2.getText().toString());

        etResultado.setText(String.valueOf(val1 - val2));
    }
}
